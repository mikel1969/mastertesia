#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/stat.h>
#include <homotopia.h>
#include <mathlink.h>

#include <common-homotopia.c>

//val_type mu0= <* mikmu *>

val_type mu0=<* mu *>;
val_type x0=<* x0 *>;
//val_type y0=0;
val_type vx0=<* vx0*>;
val_type vy0=<* vy0*>;
int N0=<* 2 m*>;

int main(int argc, char *argv[])
{
return MLMain(argc, argv);
}
