
void mathHomotopia P((double *v, long vlen, double init, double end, double *preal, long prelem, int *pint, long pielem));

:Begin:
:Function:       mathHomotopia
:Pattern:        mathHomotopia[ v_List, init_Real, end_Real, preal_List, pint_List]
:Arguments:      {v, init, end, preal, pint}
:ArgumentTypes:  { RealList, Real, Real, RealList, IntegerList }
:ReturnType:     Manual
:End:

:Evaluate: mathHomotopia::usage = "mathHomotopia[{v1,v2,v3...vn}, t0, t1, {preal0, preal1...prealm}, {pint1, pint2...pints}] starting with V0={v1,v2,v3...}, which is the solution of f(t0,V0)=0, transforms the vector V0 to the solution of the homotopy equations given by function f(t1,V). The retunr values is the list {V, t0, tend, f(tend,V)}, where tend is a value between t0 and t1 (if all has gone OK it will be t1). The jacobian of f is needed in the Newton iterations made in the continuation path and the jacobian of f is given by jacobian(), but it can be avoided, in which case the nummerical approximation of the jacobian will be used instead. The function f and its jacobian can be created by homotopiarakoFuntzioakSortu[vars, sol0, rest, jac], which creates the C code file homotopiarakofuntzioak.c. Another option is to create manually the file with both function calls (or only the function evaluation code with no jacobian). This file and the file math-homotopia.c must be compiled and the executable file must be installed in the mathematica file (call to Install of MathLink), the lists of parameters preal and pint can be used in the function and in the jacobian, as they will be passed throught. The file homotopia.conf can be used to adjust the parameters of the homothopy process. You can get more information in the file README.txt (declaration of the functions, their parameters and the configurable values for the Newton iterations)"
