#define LOGFILE
//#include <lapack.h>

/* mikel
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/stat.h>
#include <homotopia.h>
#include <mathlink.h>
/* end mikel*/

 /*
  int matrix_order, 
  lapack_int m, 
  lapack_int n,
  lapack_complex_double* a,
  lapack_int lda,
  lapack_int* ipiv
*/
//#ifdef LOGFILE
FILE *loga;
//#endif

/* mikel */
//#define NULL '\n';
//typedef double val_type;
//typedef double time_type;
/* end mikel */



typedef struct vallistelem
    {
    val_type *val;
    val_type *dval;
    double dnorma;
    struct vallistelem *sig;
    } vallistelem;

typedef struct
    {
    vallistelem * first;
    int num;
    }vallist;
    


double acc;  // fitxategitik alda daiteke
int iter_opt; // fitxategitik alda daiteke: optimun number of iterations in the newton method aplication. 
		// The deltaT will be adecuated depending in this value. The goal is to get the deltaT 
		// that produces convergence in iter_opt iterations.
double prec;    // when Newton-method does not get the accuracy required in maxstepnumber iterations
                // we see if the norm of the increment is less than pow(10,prec) in wich case 
                // we accept the solution as new root  
static int maxnumstepswithsamejac;  // 1 --> the same jacobian will be used in next step (2 steps with same jacobian)
				    // 2--> it will try to use two more times the same jacobian
                                    // 0 --> allways will get the new jacobian
static int numstepswithsamejac;
int maxdeltacondfails;
int approxjacobian;

void printmat(int dim, double *jac)
{
int i,j;

printf("\n");
for (i =0; i<dim; i++)
  {
  for (j=0; j<dim; j++) printf("%lg ",jac[i*dim+j]);
  printf("\n");
  }
}

void getdim(int *nptr);


void jacobian(int dim, val_type *varvals, time_type t, val_type *jac, double *preal, long prelem, int *pint, long pielem);
void rest(int dim, val_type *varvals, time_type t, val_type *res, double *preal, long prelem, int *pint, long pielem);

void getinitvals(int *dim,val_type *aldagaibalioak,int *pint, long pielem);

void Newjacobian(int dim, val_type *varvals, time_type t, val_type *jac, double *preal, long prelem, int *pint, long pielem)
{
int i,j;
double h;
val_type *fxminus, *fxplus,ori;

if (!approxjacobian)
    jacobian(dim,varvals,t,jac, preal, prelem, pint, pielem);
  else
  {
  h = 0.00001;
  fxminus = (val_type *)malloc(dim *sizeof(val_type));
  fxplus = (val_type *)malloc(dim *sizeof(val_type));
  if ((fxplus == NULL)||(fxminus ==NULL))
    {
    perror("NewJacobian: no memory! to abort...");
    getchar();
    exit(-1);
    }
  for (i=0; i<dim; i++)
    {
    ori = varvals[i];
    varvals[i]-=h;
    rest(dim,varvals,t,fxminus, preal, prelem, pint, pielem);
    varvals[i] = ori+h;
    rest(dim,varvals,t,fxplus, preal, prelem, pint, pielem);
    varvals[i] = ori;
    for (j=0; j<dim; j++) 
        // be carefull!! we need traspossed matrix...
        jac [i*dim + j] = (fxplus[j] -fxminus[j])/(2*h);
    }
  free(fxminus);
  free(fxplus);
  } 
//printmat(dim,jac);
}

val_type Sqrt(val_type v)
{
return(sqrt(v));
}

val_type Cot(val_type v)
{
val_type res;

res = tan(v);
return(1/res);
}

val_type Tan(val_type v)
{
val_type res;

res = tan(v);
return(res);
}

val_type Power(val_type v, double exp)
{
val_type res;

res = pow(v,exp);
return(res);
}

val_type Norm(int dim, val_type *v)
{
int i;
val_type emaitza;

emaitza = 0;
for (i = 0; i<dim; i++)
    {
    #ifdef LAPACKDOUBLE
    emaitza += v[i] * v[i];
    #elif LAPACKCOMPLEX
    auskalo_zer();
    #endif
    }
#ifdef LAPACKDOUBLE
emaitza  = sqrt(emaitza);
#elif LAPACKCOMPLEX
auskalo_zer_moduloa();
#endif
//dznrm2_(&dim,v,&emaitza);
return(emaitza);

}


void vectordiff(int n, val_type * v1, val_type * v2, val_type * v3) // v3 = v1-v2
{
while( n > 0 ) 
  {
  n--;
  v3[n] = v1[n] - v2[n];
  }
}
void vectorcp(int n, val_type * v1, val_type * v2) // v2 = v1
{
while( n > 0 ) 
  {
  n--;
  v2[n] = v1[n];
  }
}

void store (int n, val_type *v, val_type *dv, vallist *lptr)
{           // stores the pair (v,dv) in the head of the list pointed by lptr
            //   the Norm of dv is also stored.
vallistelem *aptr;

aptr = (vallistelem *)malloc(sizeof(vallistelem));
aptr->val = (val_type *)malloc(n * sizeof(val_type));
aptr->dval = (val_type *)malloc(n * sizeof(val_type));
aptr->sig = lptr->first;
vectorcp(n,v,aptr->val);
vectorcp(n,dv,aptr->dval);
aptr->dnorma = Norm(n,dv);
lptr->num ++;
//if (lptr->num == 1) lptr->dnorma = aptr->dnorma;
lptr->first = aptr;
}

void listfree(vallist *lptr)
{
vallistelem * vle,*vleaux;

for (vle = lptr->first; vle != 0; vle=vleaux)
    {
    vleaux = vle->sig;
    free(vle->val);
    free(vle->dval);
    free(vle);
    }
lptr->first = 0;
lptr->num = 0;
}

int mindnormas(int numelems, vallist vl, double *minptr)
{    // it takes into acount the last numelems of the list vl
     // it puts into var ponted by minptr the minimum value of the dnorma 
     //     values of last numelems of vl. Take into acount that the list 
     //     is in the order last is first.
     // and returns the number of elements used: numelems or, in the case 
     //    there are less elements in the list, the number of elements of vl
vallistelem *aptr;
int usedelems;

aptr = vl.first;
usedelems = vl.num;
/* if the list were in the correct order..

if (usedelems > numelems) usedelems = numelems;
    else numelems = usedelems;
// now both are equal and the value is the maximum posible:
//   if there were less than numelems both vars have the number of 
//   elems of the list. In othe case both have numelems value 
if ((numelems == 0)||(aptr == 0)) 
    {
    *minptr = 0;
    numelems = 0;
    }
  else 
    {
    *minptr = aptr->dnorma;
    numelems--; // we take the norm of the first, so it has been used
    }
for (aptr = aptr->sig; (aptr != 0)&& (numelems > 0); numelems --;aptr = aptr->sig)
        if (*minptr > aptr-> dnorma) *minptr = aptr->dnorma;

but as the list is in the opossite order ... */
while ((usedelems > numelems)&&(aptr != 0)) 
    {
    aptr = aptr->sig;
    usedelems --;
    }
if ((usedelems == 0)||(aptr ==0)) *minptr = 0;
  else
    {
    *minptr = aptr->dnorma;
    for (aptr = aptr->sig; aptr != 0; aptr = aptr->sig)
        if (*minptr > aptr-> dnorma) *minptr = aptr->dnorma;
    }
return(usedelems);
}

int  hasnan(int dim, val_type *jac0)
{
int res;
int i;

dim = dim*dim;
res=0;
for (i=0; (i<dim) && !res; i++)
    if (isnan(jac0[i]))
             res = 1; 
return(res);
}

int  vhasnan(int dim, val_type *v)
{
int res;
int i;

res=0;
for (i=0; (i<dim) && !res; i++)
    if (isnan(v[i]))
             res = 1; 
return(res);
}

static int numjac=0;

int findrootbysnewton(int dim, val_type *jac0, int *ipiv, time_type t, val_type *bal0, val_type *bal1,
		      double *preal, long prelem, int *pint, long pielem, int maxnewtoniters, double *factorptr)
/*==========================================================================================================
 *
 *      Mikel 24-10-2012 (emaitzak zehaztu ditut)       
 *
 *      Return values:
 *          = 1. Root has been found
 *          = 2. nanfound problem
 *          = 3  could not factorize jacobian
 *          = 0. maximal number of iterations reached.
 *               or (normdelta < 0.8*normdelta_prev)
 *
 *==========================================================================================================*/
{                
int i,j,k;
int nanfound;
int deltacondfails;
double tol;
val_type *delta, *newbal, *delta_prev;
//val_type *resvec;
 double normdelta,normdelta0,normdelta_prev;
 double normfx; //,normfx_prev,
double auxnorm;
int nsys, info;
 int deltacond,acccond;
double numdnormas; //it can be int
double min,ratiodnorma;
char trans;
//vallist listofvals;
int lda,ldb,iters;


//printf("starting root finding by simplified newton\n");
lda = dim;
ldb = dim;
//listofvals.num =0;
//listofvals.first = 0;
trans = 'N';
nsys = 1;   // number of systems to be solved each time by zgetrs_
if ((delta = (val_type *)malloc (dim*sizeof(val_type))) == NULL)   // Vector dim n
	perror("error allocating memory for v1 of newton iteration");
if ((newbal = (val_type *)malloc (dim*sizeof(val_type))) == NULL)   // Vector dim n
	perror("error allocating memory for v2 of newton iteration");
if ((delta_prev = (val_type *)malloc (dim*sizeof(val_type))) == NULL)   // Vector dim n
	perror("error allocating memory for v3 of newton iteration");
//resvec = (val_type *)malloc (dim*sizeof(val_type));   // Vector dim n
info = 0;
vectorcp(dim,bal0,newbal);
#ifdef LOGFILE
//fprintf(loga,"findrootbysnewton, initially the root is approximated by, t= %lg:\n",t);
//for (i = 0;i<dim; i++) fprintf(loga,"%lg,  ",bal0[i]);
#endif
rest(dim, bal0, t, delta, preal, prelem, pint, pielem);             // delta = F(bal0)
#ifdef LOGFILE    
normfx=Norm(dim,delta);   
fprintf(loga,"\ninitial rest_norm = %.16lf, with t = %lf\n F(bal0) =",normfx,t);
//for (i = 0; i< dim; i++) fprintf(loga,"%lg ",delta[i]);
#endif
if (maxnumstepswithsamejac == numstepswithsamejac)
  {
  numjac ++;
  numstepswithsamejac = 0;
  printf("COMPUTING %d JACOBIAN.............",numjac);fflush(stdout);
  Newjacobian(dim, bal0, t, jac0, preal, prelem, pint, pielem);

  fprintf(loga,"\nJac = %.16lf",Norm(dim*dim,jac0));

  printf("done!");fflush(stdout);
  // The LU decomposition of the jacobian is done only at the beginning
  printf("LU factorization...");fflush(stdout);
  #ifdef LAPACKDOUBLE
  dgetrf_( &dim,&dim, jac0, &dim, ipiv,&info);
  #elif LAPACKCOMPLEX
  zgetrf_( &dim,&dim, jac0, &dim, ipiv,&info);
  #endif
  printf("done! info = %d (should be 0)",info);fflush(stdout);
  }
  else
  {
  printf("we are going to reuse the jacobian.");fflush(stdout);
  numstepswithsamejac ++;
  }
if (info !=0)
    {
    //for (i = 0; i< dim; i++) printf ("%d --> %lg\n",i,jac0[i*dim+i]);
    free(newbal);
    free(delta);
    free(delta_prev);
    return(3);
    }
iters = 0;
nanfound = 0;
deltacond =1;
deltacondfails=0;
normdelta = 0;
//printf("acc = %lg ", acc);fflush(stdout);
tol = 0.000000001;
acccond = 1;
while ( (nanfound == 0) &&
        (info == 0) && 
        (deltacond == 1) && //(normdelta < 0.8*normdelta_prev) &&
        //(tol < normfx) &&
        (acccond == 1) && //(normdelta/normdelta0 > pow(10.0,-acc)) &&
        (iters < maxnewtoniters))
    {
    printf("+");fflush(stdout);
#ifdef LOGFILE
    fprintf(loga,"in the iterations... %d iteration\n",iters);
#endif
    iters ++;
#ifdef LAPACKDOUBLE
    dgetrs_( &trans, &dim, &nsys, jac0, &lda, ipiv, delta, &ldb, &info ); // Solve: Jac . delta = F(bal0)
    nanfound =vhasnan(dim,delta); 
    if (nanfound) 
        {
	printf("NAN found!! %d iteration\n",iters);
	fprintf(loga,"NAN found!! %d iteration\n",iters);
    #ifdef LOGFILE
	for (i = 0; i< dim; i++) fprintf(loga,"%lg ",delta[i]);
    #endif
	}
#elif LAPACKCOMPLEX
    zgetrs_( &trans, &dim, &nsys, jac0, &lda, ipiv, delta, &ldb, &info ); // Solve: Jac . delta = F(bal0)
    nanfound = 0;
#endif
    normdelta_prev = normdelta;
    normdelta = Norm(dim,delta);   //normdelta  = ||delta||
    //printf("||delta|| = %.16lg ", normdelta);fflush(stdout);
    if (iters == 1) normdelta0 = normdelta;
    if (iters > 1) 
      {
	if (normdelta > (0.8 * normdelta_prev))  // after second iteration we control 
                                                // wether the change decrements at each iteration. If do not, we will stop
	    {
	        //if (normdelta > normdelta_prev) 
			deltacondfails ++;
		printf("deltacondfails = %d (%lg > %lg)",deltacondfails,normdelta,0.8 * normdelta_prev);fflush(stdout);
		if (deltacondfails > maxdeltacondfails) deltacond = 0;
	    }
	if (normdelta/normdelta0 < pow(10.0,-acc)) acccond = 0; 
      }
    vectordiff(dim,newbal,delta, &(bal1[0]));                           // bal1 = newbal - delta
#ifdef LOGFILE
//    fprintf(loga,"new approximation\n");
//    for (i=0;i<dim;i++) fprintf(loga,"%.16lf ",bal1[i]);
    fprintf(loga," end appox.\n");
#endif 
    //store(dim, newbal, delta, &listofvals);
    vectorcp(dim,bal1,newbal);   // newbal = bal1;  newbal is the new approximation of the root
    vectorcp(dim,delta,delta_prev);   //delta_prev  = delta;
    //printf("second rest ");
    rest(dim, newbal, t, &(delta[0]), preal, prelem, pint, pielem);                                 // delta = F(newbal)   
#ifdef LOGFILE
    //normfx_prev = normfx;
    normfx = Norm(dim,delta);
    fprintf(loga,"new |F(X)| = %lg\n", normfx);
#endif
    } //END OF NEWTON ITERATIONS
/*
if (normfx <= tol) printf(" end of the Newton iterations because the root has been found! F(x)= %lg\n",normfx);
    else 
      {
      printf(" end of Newton iterations: nanfound = %d, info = %d, deltacond = %d, iters = %d, F(x) = %lg\n", nanfound, info, deltacond, iters,normfx);
      info = 3;
      }
*/
//printf("end newton iterations: nanfound %d info %d deltacond %d acccond %d iters %d ", nanfound, info, deltacond, acccond, iters); fflush(stdout);
#ifdef LOGFILE
if (nanfound != 0) fprintf(loga,"end of the newton iterations because NaN on solving equationd by lapack\n");
if (info != 0) fprintf(loga,"end of the newton iterations by info != 0\n");
if (deltacond == 0) fprintf (loga,"end newton iterations because difference between approximations do not decrease after %d iterations\n",iters);
//if (normfx <= tol) fprintf(loga," end of the Newton iterations because the root has been found! F(x)= %lg\n",normfx);
//if (Norm(dim,dbal1)/alpha > pow(10.0,-acc)) printf ("end newton iterations by cause 3\n");
if (iters >= maxnewtoniters) fprintf (loga,"end newton iterations by cause 4 (too many iterations...)\n");
#endif
 if ((info == 0) && (nanfound ==0)&& ((acccond == 0) || (normdelta < pow(10.0,-prec))) )
    {
    info = 1;
#ifdef LOGFILE
    fprintf(loga,"the new root is:\n");
    //for (i = 0;i<dim; i++) fprintf(loga,"%lg,  ",bal1[i]);  
    fprintf(loga,"\n"); 
    //getchar ();
#endif
    free(newbal);
    free(delta);
    free(delta_prev);
    /*numdnormas=mindnormas(4,listofvals, &min); // numdnormas = Min(4,listofvals.num);
                                             // min = Min_dnorma(Take(listofvals,numdnormas))
    //printf("minimum norm of the list =%lg, and number of elements used: %lf\n",min,numdnormas);
    if (min == 0)
        {
	*factorptr = 0;
        }
      else
        {
        ratiodnorma =min/normdelta_prev; //?????????
        if (ratiodnorma > pow (10.0, -acc))
            {
            info = 0;
            *factorptr = 0;
            }
          else
            *factorptr = 1.0/pow(ratiodnorma,1.0/(numdnormas-1));
        }
    */
    *factorptr = pow(10,acc*(1.0/(double)iters - 1.0/(double)iter_opt));
		     //listfree(&listofvals);
    return (1);
    }
  else 
    {  
    *factorptr = 0;
    printf("it has been considered there is no convergence! ");fflush(stdout);
    }
//listfree(&listofvals);
free(newbal);
free(delta);
free(delta_prev);
if (nanfound)
    {
    printf("returning nanfound"); fflush(stdout);
    return(2);
    }
  else return(info);
}


int steptotaux(int dim, val_type *jac0, int *ipiv, time_type t0, time_type dt, 
                time_type t1, time_type *tnewptr, time_type *dtnewptr, 
                val_type *bal0, val_type *bal1, 
		double *preal, long prelem, int *pint, long pielem,
                double converate, int maxnewtoniters, int maxinnersteps)
{
int i,j;
time_type dtused;
int rootfound, steps;
double factor;
double dtreducefactor;
double dtfactor2reusejac;
double maxstepincr;

maxstepincr = 4.0;
dtreducefactor = 0.3;
if (!approxjacobian) dtfactor2reusejac = 0.9;
  else dtfactor2reusejac = 0.6;
rootfound = -1;
if (dt >0) 
    {
    if ((t0 + dt) > t1) dt = t1-t0;
    }
  else
    {
    if ((t0 + dt) < t1) dt = t1-t0;
    }
for (steps = 0, dtused = dt;
     (rootfound != 1) && (steps < maxinnersteps);
     steps ++)
    {
    *tnewptr = t0 + dt;
#ifdef LOGFILE
    //fprintf(loga," t = %lf to find the root\n",*tnewptr); 
#endif
    printf("    to findroot step = %d ",steps);fflush(stdout);
    rootfound = findrootbysnewton(dim, jac0,ipiv, t0+dt, bal0, bal1, preal, prelem, pint, pielem,maxnewtoniters, &factor);
    if ((rootfound != 1)&&(rootfound != 2)&&(rootfound != 3))
        {
	if (numstepswithsamejac == 0 )  // we reduce stepsize by dtreducefactor only if we are in the case with good jacobian and not convergence
		dt = dt * dtreducefactor;
	    else   // in other case  we reduce by 2*reducefactor
		{
		dt = dt * ((dtreducefactor > 0.5)?dtreducefactor:dtreducefactor*2.0);
		}
	numstepswithsamejac= maxnumstepswithsamejac;  // to force the evaluation of jacobian and LU factorization
        
        printf("trying again newton iteration with new jacobian... from t = %lg to %lg (dt = %lg)\n",t0, t0+dt, dt);
#ifdef LOGFILE
        fprintf(loga,"Newton iteration convergence failed! returned value = %d (we are going to try again with a shorter step: %lg)\n",rootfound,dt);
#endif
        }
      else  // rootfound== 1 or rootfound == 2 
        {
        if (rootfound != 1) // to force the evaluation of jacobian and LU factorization
            {
            printf("    ROOT NOT FOUND: NANFOUND or PROBLEM in FACTORIZATION...");fflush(stdout);
            numstepswithsamejac= maxnumstepswithsamejac;  
            }
        }
    }
if (rootfound == 1)
    {
    printf("findroot ok: root found!\n");
    if (factor > maxstepincr) factor = maxstepincr;
    *dtnewptr = dt *factor;// * converate;
    if (numstepswithsamejac <maxnumstepswithsamejac) 
	// if in the next newton process we are going to reuse the jacobian we will use a shorter step
	*dtnewptr = (*dtnewptr) * dtfactor2reusejac;
    }
return (rootfound);
}


/*
maxnewtoniter = 20;   //???????????????
maxinnersteps = 5;
double prec =20;
double acc = 5;
t0 = 0;
t1 = 1;
*/
int problem_init_values(int *maxNWTiter, int *maxINNERstep, time_type *t0, time_type *t1)
{
char word[81];
FILE *fitx;
struct stat stata;
int i;

approxjacobian = 0;

if (stat("./homotopia.conf",&stata) == 0) 
    {
  	fitx = fopen("./homotopia.conf" ,"r");
  	//printf("changing default values\n");
  	for (i = fscanf(fitx,"%80s",word);
     	 i != EOF;
     	 i = fscanf(fitx,"%80s",word))
         /* no more than 80 chars, it stops in a space or EOL */
    	{
    	if (i == 1)  /* it has read the word */
      	   {
           if ((word[0] == '\0') || (word[0] == '#'))
           		fscanf(fitx,"%*[^\n]\n"); /* comment line */
            else 
             {
             if (strcmp(word,"t0")==0) 	//Initial t #
               {
               fscanf(fitx,"%lf%*[^\n]\n",t0);
               }
              else
               {
               if (strcmp(word,"t1")==0)  //Final t #
		{
                fscanf(fitx,"%lf%*[^\n]\n",t1);
                }
               else
                {
                 if (strcmp(word,"Newton")==0) 	//Newton iteration #
               	  {
               	  fscanf(fitx,"%d%*[^\n]\n",maxNWTiter);
               	  }
                 else
              	  {
	       	  if (strcmp(word,"Inner")==0)
		       	fscanf(fitx,"%d%*[^\n]\n",maxINNERstep); 
		  else
		   {
		   if (strcmp(word,"prec")==0)
		      fscanf(fitx,"%lf%*[^\n]\n",&prec); 
	            else
	              if (strcmp(word,"acc")==0)
		         fscanf(fitx,"%lf%*[^\n]\n",&acc);
		      else
                        if (strcmp(word,"iter_opt")==0) 
			  fscanf(fitx,"%d%*[^\n]\n",&iter_opt);
		        else
                          if (strcmp(word,"maxnumstepswithsamejac")==0) 
			    fscanf(fitx,"%d%*[^\n]\n",&maxnumstepswithsamejac);
		           else
                            if (strcmp(word,"maxdeltacondfails")==0) 
			      fscanf(fitx,"%d%*[^\n]\n",&maxdeltacondfails);
				else
                                 if (strcmp(word,"approxjacobian")==0) 
					approxjacobian = 1;
	            } // else Inner
           	  } // else Newton
                 } // else t1
                } //else t0
              } //else  not comment
       	   } //else if 1==1
       	} /* for*/
    fclose(fitx);
    }
  else /* file with initial values no present! */	
	{ 
	}
return(0);
}

void mathHomotopia(double *bal0, long n,double init, double end, double *preal, long prelem, int *pint, long pielem)
{
int k;
int i,j,max_steps,nstep,maxinnersteps;
int info;
time_type dt,dtnew,t00,t01,taux,tnew;
time_type t0,t1;
//val_type *bal0;
val_type *bal1;
val_type *res0;
val_type *jac0;
int *ipiv;
int rootfound;
double norma, normaz, converate,norma2, norma3,normaux;
double auxfactor;
int maxnewtoniter;
char norma_type;
long m;
//unsigned long long raw = 0x7ff8000000000000;
//unsigned long long raw1 = 0xfff8000000000000;

// mikel 07-02-2013
// dimentsioa ondo dagolea konprobatuz
m=pint[0];
if (n != (8*m)+9)
 { 
   printf("Errorea: dimentsioa okerra da\n");
   return ;
 }

loga = fopen("./mathHomotopia.log","w");
fprintf(loga,"math Homotopiaren hasiera...\n");
//#endif
t0 = init;
t1 = end;
norma_type = 'F';
maxnewtoniter = 20;   //???????????????
maxinnersteps = 5;
prec =13;
acc = 3;
 iter_opt= 8;
t00 = 0;
t01 = 0;
maxdeltacondfails=2;
maxnumstepswithsamejac=2; // 0 -> the jacobian is computed for every newton method
problem_init_values(&maxnewtoniter,&maxinnersteps, &t00, &t01);
if (t00 != 0) t0 = t00;
if (t01 != 0) t1 = t01;

if ((bal1 = (val_type *)malloc ((n)*sizeof(val_type)))==NULL)  // Vector dim n
    {
    t1 = t0;
#ifdef LOGFILE
    fprintf(loga,"malloc problem! tfinal = %lf\n",t0);
#endif
    }
if ((res0 = (val_type *)malloc (n*sizeof(val_type)))==NULL)  // Vector dim n
    {
    t1 = t0;
#ifdef LOGFILE
    fprintf(loga,"malloc problem! tfinal = %lf\n",t0);
#endif
    }
if ((ipiv = (int *)malloc (n*sizeof(int)))==NULL)  // Vector dim n
    {
    t1 = t0;
#ifdef LOGFILE
    fprintf(loga,"malloc problem! tfinal = %lf\n",t0);
#endif
    }
//getinitvals(bal0);
max_steps =400;
dt = (t1-t0)/max_steps;
// mikel
//dt = 0.005;
if ((jac0 = (val_type *)malloc (n*n*sizeof(val_type)))==NULL) // n x n Matrix
    {
    t1 = t0;
#ifdef LOGFILE
    fprintf(loga,"malloc problem! tfinal = %lf\n",t0);
#endif
    }


/*
REAL FUNCTION CLANGE( NORM, M, N, A, LDA, WORK )
           CHARACTER NORM  ---> M (maximoa) 1 (1 norma) I (infinitoa) F (frobenius)
           INTEGER   LDA, M, N   -> dim, dim, dim
           REAL      WORK( * )   -> only for I (infinitoa), array of real dimension M
           COMPLEX   A( LDA, * ) -> Matrix  dim x dim
returns the value of the one norm, or the Frobenius norm, or
       the infinity norm, or the element of largest absolute value of  a  com‐
       plex matrix A 

REAL FUNCTION SLANGE( NORM, M, N, A, LDA, WORK )
returns the value of the one norm, or the Frobenius norm, or
       the infinity norm, or the element of largest absolute value of  a  real
       matrix A
*/
#ifdef LOGFILE
fprintf(loga, "argumentoak: n: %li eta nhelb: %p\n", n, &n);
#endif
for (taux = t0, nstep = 0, numstepswithsamejac=maxnumstepswithsamejac;
     ((dt>0)?(taux-t1 < 0):(t1-taux < 0)) && (nstep < 10*max_steps); taux = tnew, nstep ++,dt=dtnew)
    {
    printf("trying to walk: step= %d,  from t = %lg to t= %lg with dt = %lg \n",nstep, taux,taux+dt,dt);
#ifdef LOGFILE
    fprintf(loga,"I' ll try to walk:  step= %d,  from t = %lg to t= %lg with dt = %lg \n",nstep, taux,taux+dt,dt);
#endif
    rootfound = steptotaux(n, jac0, ipiv, taux, dt, t1, &tnew, &dtnew, bal0, &(bal1[0]),preal,prelem,pint,pielem, converate, maxnewtoniter, maxinnersteps);
    if ((rootfound < 1) || (rootfound >=2))
        {
        if (rootfound ==2) fprintf(loga,"Lapck reurned solution with NaN when t == %lg\n",taux);
         else 
          if (rootfound == 0)  fprintf(loga,"You have a problem: try to reduce the accuracy or increase working precision...\n");
            else fprintf(loga,"some problem in the %d argument when solving the lineal system of a s-Newton step...\n", -rootfound);
	t1 = taux;  // taux is the last t for which the root has been found. This root, bal0, will be the solution given.
        tnew = t1;  // this is a way to go out from the for
        }
      else
        {
#ifdef LOGFILE
        fprintf(loga,"mathHomotopia: root found (else way). Now to compute new jacobian the jacobian for next step:\n");
#endif
        vectorcp(n,bal1,&(bal0[0]));
	normaz = norma;
	#ifdef LAPACKDOUBLE
        //norma kalkulatzeko: 
        norma = slange_(&norma_type, &n, &n, jac0, &n, ipiv);
	#elif LAPACKCOMPLEX
        //norma kalkulatzeko: 
        norma = clange_(&norma_type, &n, &n, jac0, &n, ipiv);
	#endif
        //dtnew = dtnew * normaz/norma;
	if (dt > 0)
            {
            if ((t1 - tnew) < dtnew) dtnew = t1-tnew;
            }
          else
            {
            if ((t1 - tnew) > dtnew) dtnew = t1-tnew;
            }
	//printf("urrats berrira: t = %lg, dt = %lg\n",tnew,dtnew);
        }
#ifdef LOGFILE
    fprintf(loga," STEP %d .... rootfound = %d ...\n",nstep,rootfound);
#endif
    }
acc=acc*2;
numstepswithsamejac=maxnumstepswithsamejac;
rootfound = findrootbysnewton(n, jac0,ipiv, t1, bal0, &(bal1[0]),preal,prelem,pint,pielem,maxnewtoniter, &auxfactor);
if (rootfound != 1) {
    fprintf(loga, "Kontuz: azken urratsean zerbait gaizki joan da!!!!!\n");
   }
printf("number of jacobian computations = %d\n",numjac);
// emaitzak eman
/*
printf("new root:\n");
for (i = 0;i<n; i++) printf("%.12lg\n",bal1[i]);
printf(" and the norm of the rest of the root is ");
rest(n, bal1, tnew, res0);
printf(" %lg (at t = %lg)\n",Norm(n,res0),tnew);
*/
rest(n, bal1, t1, res0, preal, prelem, pint, pielem);
norma3 = Norm(n,res0);
printf("hondarra bukaeran: ||F(x)|| =%lg, t=%lg \n", norma3,t1);
fprintf(loga,"hondarra bukaeran: ||F(x)|| =%lg, t=%lg \n", norma3,t1);
//for (i = 0;i<n; i++)
//    { fprintf(loga,"%lg,  ",res0[i]);}  
printf("new root:\n");
for (i = 0;i<n; i++) fprintf(loga,"%.12lg,",bal1[i]);
fprintf(loga,"\n");


if (!MLPutFunction(stdlink, "List",4))
    {
    fprintf(loga,"MLPutFunction: error when returning result to Mathematica!!!!\n");
    MLErrorMessage(stdlink);
    };
if (!MLPutReal64List( stdlink, bal1, n)) 
    {
    fprintf(loga,"MLPutReal64List: error when returning result to Mathematica!!!!\n");
    MLErrorMessage(stdlink);
    }
if (!MLPutReal64( stdlink,t0)) 
    {
    fprintf(loga,"MLPutReal64: error when returning result to Mathematica!!!!\n");
    MLErrorMessage(stdlink);
    }
if (!MLPutReal64( stdlink,t1)) 
    {
    fprintf(loga,"MLPutReal64: error when returning result to Mathematica!!!!\n");
    MLErrorMessage(stdlink);
    }
if (!MLPutReal64( stdlink,norma3)) 
    {
    fprintf(loga,"MLPutReal64: error when returning result to Mathematica!!!!\n");
    MLErrorMessage(stdlink);
    }
if(! MLEndPacket(stdlink))
    {
    fprintf(loga,"MLEndPacket: error when returning result to Mathematica!!!!\n");
    MLErrorMessage(stdlink);
    }
if(! MLFlush(stdlink))
    {
    fprintf(loga,"MLFlush: error when returning result to Mathematica!!!!\n");
    MLErrorMessage(stdlink);
    }
free(jac0);
free(ipiv);
free(res0);
free(bal1);
fprintf(loga,"end of mathHomotopia!\n");
fclose(loga);
}


