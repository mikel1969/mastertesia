#include <stdio.h>
#include <homotopia.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <erabiltzaileak.c>

#define Power(x, y)	(pow((double)(x), (double)(y)))
#define Sqrt(x)		(sqrt((double)(x)))

#define Abs(x)		(fabs((double)(x)))

#define Exp(x)		(exp((double)(x)))
#define Log(x)		(log((double)(x)))

#define Sin(x)		(sin((double)(x)))
#define Cos(x)		(cos((double)(x)))
#define Tan(x)		(tan((double)(x)))

#define ArcSin(x)       (asin((double)(x)))
#define ArcCos(x)       (acos((double)(x)))
#define ArcTan(x)       (atan((double)(x)))

#define Sinh(x)          (sinh((double)(x)))
#define Cosh(x)          (cosh((double)(x)))
#define Tanh(x)          (tanh((double)(x)))


//#define E		2.71828182845904523536029
//#define Pi		3.14159265358979323846264
//#define Degree		0.01745329251994329576924


#define Cot(a) (1/Tan(a))            // Cotangentearen definizioa
#define Csc(a) (1/Sin(a))	     // Cosecantearen definizioa


void irakurHode (int dim,int i,val_type *auxHode, val_type res[4][4])

{ int k;

for (k=0; k<4; k++)
 {
  res[0][k]=auxHode[(i*16)+k];
  res[1][k]=auxHode[(i*16)+(k+1*4)];
  res[2][k]=auxHode[(i*16)+(k+2*4)];
  res[3][k]=auxHode[(i*16)+(k+3*4)];
  }

return;

}
		                                       
val_type Mjl(int dim,int j, int l)

{
int k;
val_type kx;
val_type tj,tl, batura,e;
val_type N0,M;


N0=(dim-5)/4;
M=(N0-1)/2;

tj=(2*Pi*j)/(N0);
tl=(2*Pi*l)/(N0);

batura=0;

for (k=1; k<(M+1); k++) {
     kx=k;
     batura+=(1/kx)*Sin(kx*(tl-tj));}

e=-(1/(Pi*N0))*batura;
return(e);

}
 


void Fode(val_type pq[],val_type mu,val_type lambda,val_type res[])
{

int T;
val_type e1[4],e2[4];
val_type tau;

T=1;
tau=0;

edofun(tau,pq,mu,T,e1);
Hfgrad(pq,mu,e2);

res[0]=e1[0]+lambda*e2[0];
res[1]=e1[1]+lambda*e2[1];
res[2]=e1[2]+lambda*e2[2];
res[3]=e1[3]+lambda*e2[3];

return;

}



void hondarra(int dim, val_type *bal, val_type mu, val_type T, val_type em[])
{

int i,j,k,l,N0;
val_type a0,b0,c0,d0,lambda,auxjl;
val_type pq[4], batura[4];
val_type fem[4];
val_type *auxfem; 


N0=(dim-5)/4;

auxfem = (val_type *)malloc((4)*(N0)*sizeof(val_type));
if (auxfem == NULL) perror("malloc errorea");

a0=bal[4*N0];
b0=bal[4*N0+1];
c0=bal[4*N0+2];
d0=bal[4*N0+3];
lambda=bal[4*N0+4];

em[4*N0+1]=0;em[4*N0+2]=0;em[4*N0+3]=0;em[4*N0+4]=0;

for (j=0; j<N0; j++) {

    pq[0]=bal[j];
    pq[1]=bal[N0+j];
    pq[2]=bal[2*N0+j];
    pq[3]=bal[3*N0+j];
         
    Fode(pq,mu,lambda,fem);
    
    auxfem[j]=fem[0];
    auxfem[N0+j]=fem[1];
    auxfem[2*N0+j]=fem[2];
    auxfem[3*N0+j]=fem[3];

 }
 

for (j=0; j<N0; j++) {
    
    batura[0]=0;batura[1]=0;batura[2]=0;batura[3]=0;
    
    for (l=0; l<N0; l++) {
                         
         auxjl=Mjl(dim,j,l);

	 batura[0]+=auxjl*auxfem[l];
	 batura[1]+=auxjl*auxfem[N0+l];
	 batura[2]+=auxjl*auxfem[2*N0+l];
	 batura[3]+=auxjl*auxfem[3*N0+l];

     }


     pq[0]=bal[j];
     pq[1]=bal[N0+j];
     pq[2]=bal[2*N0+j];
     pq[3]=bal[3*N0+j];

     em[j]=pq[0]-a0-T*batura[0];
     em[N0+j]=pq[1]-b0-T*batura[1];
     em[2*N0+j]=pq[2]-c0-T*batura[2];
     em[3*N0+j]=pq[3]-d0-T*batura[3];

     em[4*N0+1]+=auxfem[j];
     em[4*N0+2]+=auxfem[N0+j];
     em[4*N0+3]+=auxfem[2*N0+j];
     em[4*N0+4]+=auxfem[3*N0+j];

  } 

em[4*N0]=bal[N0];       /*qb0*/

free (auxfem);
return;

}      
  

void japexac (int dim,val_type *x,val_type *jak,val_type mu, val_type T)

{
int i,j,k,l,N0;
val_type grad[4];
val_type Hode[4][4];
val_type pq[4],batura[4];
val_type a0,b0,c0,d0,lambda,auxmij;
val_type *auxHode; 

auxHode = (val_type *)malloc((16)*(dim)*sizeof(val_type));
if (auxHode == NULL) perror("malloc errorea");


N0=(dim-5)/4;

a0=x[4*N0];
b0=x[4*N0+1];
c0=x[4*N0+2];
d0=x[4*N0+3];
lambda=x[4*N0+4];


/* hasieraketak eta aurrekalkuluak*/

for (i=0; i<dim; i++)
{ for (j=0; j<dim; j++)
   { 
     jak[(dim)*i+j]=0;
   }
   
   pq[0]=x[i];
   pq[1]=x[N0+i];
   pq[2]=x[2*N0+i];
   pq[3]=x[3*N0+i];
   fodegraf(pq, mu, lambda,Hode);
 
   for (k=0; k<4; k++)
    { auxHode[(i*16)+k]=Hode[0][k];
      auxHode[(i*16)+(k+1*4)]=Hode[1][k];
      auxHode[(i*16)+(k+2*4)]=Hode[2][k];
      auxHode[(i*16)+(k+3*4)]=Hode[3][k];
    } 
   
}



      
/* 1-nagusiren deribatuak */

for (i=0; i<N0; i++)
  {
    batura[0]=0;
    batura[1]=0;
    batura[2]=0;
    batura[3]=0;

    for (j=0; j<N0; j++)
       {
         pq[0]=x[j];
         pq[1]=x[N0+j];
         pq[2]=x[2*N0+j];
         pq[3]=x[3*N0+j];

         irakurHode(dim,j,auxHode,Hode);

 
         auxmij=Mjl(dim,i,j);
    
         if (i==j)
           { jak[(dim*i)+(j)]=1;
             jak[dim*(i+N0)+(j+N0)]=1;
             jak[dim*(i+2*N0)+(j+2*N0)]=1;
             jak[dim*(i+3*N0)+(j+3*N0)]=1;

           }
         else
           { 
             for (k=0; k<4; k++)
                {
                  jak[(dim*i)+(k*N0+j)]=-T*auxmij*Hode[0][k];
		  jak[dim*(i+N0)+(k*N0+j)]=-T*auxmij*Hode[1][k];
                  jak[dim*(i+2*N0)+(k*N0+j)]=-T*auxmij*Hode[2][k];
                  jak[dim*(i+3*N0)+(k*N0+j)]=-T*auxmij*Hode[3][k];               
                }
           }

         Hfgrad(pq, mu, grad); 
         batura[0]+=auxmij*grad[0];
         batura[1]+=auxmij*grad[1];
         batura[2]+=auxmij*grad[2];
         batura[3]+=auxmij*grad[3];

        } //for-j end
 
      /* lambda deribatua */
    
     for (k=0; k<4; k++)
      {
        jak[dim*(i+k*N0)+(4*N0+4)]=-T*batura[k];
      }
    
     /* a0,b0,c0,d0 deribatuak */
     jak[(dim*i)+(4*N0)]=-1;
     jak[dim*(i+N0)+(4*N0+1)]=-1;
     jak[dim*(i+2*N0)+(4*N0+2)]=-1;
     jak[dim*(i+3*N0)+(4*N0+3)]=-1;
    
   }  //for-i end


/* qb0=0 funtzioaren deribatua */

jak[(dim*4*N0)+N0]=1;

/* Sumfi-ren deribatuak*/

batura[0]=0;
batura[1]=0;
batura[2]=0;
batura[3]=0;

for (l=0; l<N0; l++)
    {
       pq[0]=x[l];
       pq[1]=x[N0+l];
       pq[2]=x[2*N0+l];
       pq[3]=x[3*N0+l];
       irakurHode(dim,l,auxHode,Hode);

       for (k=0; k<4; k++)
                {
                  jak[dim*(4*N0+1)+(k*N0+l)]=Hode[0][k];
		  jak[dim*(4*N0+2)+(k*N0+l)]=Hode[1][k];
                  jak[dim*(4*N0+3)+(k*N0+l)]=Hode[2][k];
                  jak[dim*(4*N0+4)+(k*N0+l)]=Hode[3][k];               
                }

	Hfgrad(pq, mu, grad); 
        batura[0]+=grad[0];
        batura[1]+=grad[1];
        batura[2]+=grad[2];
        batura[3]+=grad[3];
	
    }

jak[dim*(4*N0+1)+(4*N0+4)]=batura[0];
jak[dim*(4*N0+2)+(4*N0+4)]=batura[1];
jak[dim*(4*N0+3)+(4*N0+4)]=batura[2];
jak[dim*(4*N0+4)+(4*N0+4)]=batura[3];       

free (auxHode);
return;
}



// 07-02-2013 Mikel: borratzeko??
void getdim(int *nptr)
{//*nptr =DM;
   *nptr =1603;}

int getinitvals(int *dim,val_type *aldagaibalioak,int *pint, long pielem)
{
/* 
 dim aldagaian fbalioak.dat fitxategian elementu kopurua itzultzen du
*/

int j,k,N0;
long m;
val_type T0; 
val_type lambda01 =0;
val_type lambda02 =0;
val_type tmp;
FILE *fbalioak;

m=pint[0];
N0=2*m+1;

fbalioak= fopen("fbalioak.dat", "r");

if (fbalioak == NULL)
 { 
   printf("Errorea: ez da aurkitzen fbalioak.dat fitxategia\n");
   *dim=0;
   return (-1);
 }


k=0;

while (fscanf (fbalioak, "%lg", &tmp)!=EOF)
  {if (k<(4*N0+5))
      {aldagaibalioak[k]=(val_type) tmp;
      }
   k++;}

*dim=k;


fclose(fbalioak);
return(0) ;

}


void rest(int dim, val_type *varvals, val_type T0, val_type *res, val_type *preal,long prelem,int *pint, long pielem)
{
int j,k,N0;
val_type *balioak0;    

val_type lambda01, lambda02;
val_type mu;
val_type pq[4];


N0=(dim-5)/4;

//balioak0 = (val_type *)malloc((4)*(N0)*sizeof(val_type));
//if (balioak0 == NULL) perror("malloc errorea");

mu=preal[0];

/*for (k=0; k<4; k++)
 {
  for (j=0; j<N0; j++) 
   {
    balioak0[(N0)*k+j]=varvals[j+k*(N0)];
   }
 }
*/

hondarra(dim,varvals,mu,T0,res);

//free (balioak0);
return;

}

void jacobian(int dim, val_type *varvals, val_type T0, val_type *jac, double *preal, long prelem, int *pint, long pielem)
{
// jakobiarra "traspose"-a itzultzen du, lapack-eko paketeak horrela
// behar baitu.

int j,k,N0;
val_type *balioak0; 

val_type lambda01,lambda02;
val_type mu;
val_type pq[4];
val_type *jmat;

N0=(dim-5)/4;

jmat = (val_type *)malloc((dim)*(dim)*sizeof(val_type));
if (jmat == NULL) perror("malloc errorea");

balioak0 = (val_type *)malloc((4)*(N0)*sizeof(val_type));
if (balioak0 == NULL) perror("malloc errorea");

mu=preal[0];

/*for (k=0; k<4; k++)
 {
  for (j=0; j<N0; j++) 
   {
    balioak0[(N0)*k+j]=varvals[j+3+k*(N0)];
   }
 }
*/

japexac(dim,varvals,jmat,mu,T0);

for (k=0; k<dim; k++)
  {
   for (j=0; j<dim; j++)
       {jac[dim*k+j]=jmat[j*(dim)+k];		// traspose
       }             
  }
   
// {jac[dim*k+j]=jmat[k][j];} : jakobiar normala itzultzeko 

free (jmat);
free (balioak0);
return;

}







