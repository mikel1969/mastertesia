#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>


val_type edofun(val_type tau,
                val_type pq[], 
                val_type mu, val_type TT, val_type res[])

{
val_type x,y,vx,vy,t;
val_type e;

x=pq[0];
y=pq[1];
vx=pq[2];
vy=pq[3];

res[0] = (TT*(vx + y))/Sqrt(mu/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) + 
     (1 - mu)/Power(Power(mu + x,2) + Power(y,2),1.5));
res[1] = (TT*(vy - x))/Sqrt(mu/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) + 
     (1 - mu)/Power(Power(mu + x,2) + Power(y,2),1.5));
res[2] = (TT*(vy - (mu*(-1 + mu + x))/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) - 
       ((1 - mu)*(mu + x))/Power(Power(mu + x,2) + Power(y,2),1.5)))/
   Sqrt(mu/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) + 
     (1 - mu)/Power(Power(mu + x,2) + Power(y,2),1.5));
res[3] = (TT*(-vx - y*(mu/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) + 
          (1 - mu)/Power(Power(mu + x,2) + Power(y,2),1.5))))/
   Sqrt(mu/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) + 
     (1 - mu)/Power(Power(mu + x,2) + Power(y,2),1.5));

return(e) ;
}


val_type Hfun(const val_type pq[], val_type mu)

{
val_type x,y,vx,vy;
val_type e;

x=pq[0];
y=pq[1];
vx=pq[2];
vy=pq[3];

e = Power(vx,2)/2. + Power(vy,2)/2. - vy*x + vx*y - 
   (1.*mu)/Power(Power(1 - mu - 1.*x,2) + Power(y,2),0.5) - 
   (1.*(1 - mu))/Power(Power(mu + x,2) + Power(y,2),0.5);

return(e) ;
}



void Hfgrad(const val_type pq[4], val_type mu,val_type res[4])
{
val_type x,y,vx,vy;

x=pq[0];
y=pq[1];
vx=pq[2];
vy=pq[3];

res[0]= -vy - (1.*mu*(1 - mu - 1.*x))/
    Power(Power(1 - mu - 1.*x,2) + Power(y,2),1.5) + 
   (1.*(1 - mu)*(mu + x))/Power(Power(mu + x,2) + Power(y,2),1.5);

res[1]= vx + (0.5*mu*(0. + 2*y))/Power(Power(1 - mu - 1.*x,2) + Power(y,2),1.5) + 
   (1.*(1 - mu)*y)/Power(Power(mu + x,2) + Power(y,2),1.5);

res[2]= 0. + vx + y;

res[3]= vy - x;

return;

}

void fodegraf(const val_type pq[], val_type mu,val_type lambda,val_type res[4][4])
{
val_type x,y,vx,vy;

x=pq[0];
y=pq[1];
vx=pq[2];
vy=pq[3];


// fode1

res[0][0]=lambda*((-3.*mu*Power(1 - mu - 1.*x,2))/
       Power(Power(1 - mu - 1.*x,2) + Power(y,2),2.5) + 
      (1.*mu)/Power(Power(1 - mu - 1.*x,2) + Power(y,2),1.5) - 
      (3.*(1 - mu)*Power(mu + x,2))/Power(Power(mu + x,2) + Power(y,2),2.5) + 
      (1.*(1 - mu))/Power(Power(mu + x,2) + Power(y,2),1.5)) - 
   ((vx + y)*((-3*mu*(-1 + mu + x))/
         Power(Power(-1 + mu + x,2) + Power(y,2),2.5) - 
        (3*(1 - mu)*(mu + x))/Power(Power(mu + x,2) + Power(y,2),2.5)))/
    (2.*Power(mu/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) + 
        (1 - mu)/Power(Power(mu + x,2) + Power(y,2),1.5),1.5));
res[0][1]=lambda*(0. + (1.5*mu*(1 - mu - 1.*x)*(0. + 2*y))/
       Power(Power(1 - mu - 1.*x,2) + Power(y,2),2.5) - 
      (3.*(1 - mu)*(mu + x)*y)/Power(Power(mu + x,2) + Power(y,2),2.5)) - 
   ((vx + y)*((-3*mu*y)/Power(Power(-1 + mu + x,2) + Power(y,2),2.5) - 
        (3*(1 - mu)*y)/Power(Power(mu + x,2) + Power(y,2),2.5)))/
    (2.*Power(mu/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) + 
        (1 - mu)/Power(Power(mu + x,2) + Power(y,2),1.5),1.5)) + 
   1/Sqrt(mu/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) + 
      (1 - mu)/Power(Power(mu + x,2) + Power(y,2),1.5));

res[0][2]=0. + 1/Sqrt(mu/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) + 
      (1 - mu)/Power(Power(mu + x,2) + Power(y,2),1.5));
res[0][3]=-lambda;

// fode2

res[1][0]=lambda*((1.5*mu*(1 - mu - 1.*x)*(0. + 2*y))/
       Power(Power(1 - mu - 1.*x,2) + Power(y,2),2.5) - 
      (3.*(1 - mu)*(mu + x)*y)/Power(Power(mu + x,2) + Power(y,2),2.5)) - 
   ((vy - x)*((-3*mu*(-1 + mu + x))/
         Power(Power(-1 + mu + x,2) + Power(y,2),2.5) - 
        (3*(1 - mu)*(mu + x))/Power(Power(mu + x,2) + Power(y,2),2.5)))/
    (2.*Power(mu/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) + 
        (1 - mu)/Power(Power(mu + x,2) + Power(y,2),1.5),1.5)) - 
   1/Sqrt(mu/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) + 
      (1 - mu)/Power(Power(mu + x,2) + Power(y,2),1.5));
res[1][1]=lambda*((-0.75*mu*Power(0. + 2*y,2))/
       Power(Power(1 - mu - 1.*x,2) + Power(y,2),2.5) + 
      (1.*mu)/Power(Power(1 - mu - 1.*x,2) + Power(y,2),1.5) - 
      (3.*(1 - mu)*Power(y,2))/Power(Power(mu + x,2) + Power(y,2),2.5) + 
      (1.*(1 - mu))/Power(Power(mu + x,2) + Power(y,2),1.5)) - 
   ((vy - x)*((-3*mu*y)/Power(Power(-1 + mu + x,2) + Power(y,2),2.5) - 
        (3*(1 - mu)*y)/Power(Power(mu + x,2) + Power(y,2),2.5)))/
    (2.*Power(mu/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) + 
        (1 - mu)/Power(Power(mu + x,2) + Power(y,2),1.5),1.5));

res[1][2]=1.*lambda;
res[1][3]=1/Sqrt(mu/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) + 
     (1 - mu)/Power(Power(mu + x,2) + Power(y,2),1.5));

// fode3

res[2][0]=((3*mu*Power(-1 + mu + x,2))/Power(Power(-1 + mu + x,2) + Power(y,2),2.5) - 
      mu/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) + 
      (3*(1 - mu)*Power(mu + x,2))/Power(Power(mu + x,2) + Power(y,2),2.5) - 
      (1 - mu)/Power(Power(mu + x,2) + Power(y,2),1.5))/
    Sqrt(mu/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) + 
      (1 - mu)/Power(Power(mu + x,2) + Power(y,2),1.5)) - 
   (((-3*mu*(-1 + mu + x))/Power(Power(-1 + mu + x,2) + Power(y,2),2.5) - 
        (3*(1 - mu)*(mu + x))/Power(Power(mu + x,2) + Power(y,2),2.5))*
      (vy - (mu*(-1 + mu + x))/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) - 
        ((1 - mu)*(mu + x))/Power(Power(mu + x,2) + Power(y,2),1.5)))/
    (2.*Power(mu/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) + 
        (1 - mu)/Power(Power(mu + x,2) + Power(y,2),1.5),1.5));
res[2][1]=lambda + ((3*mu*(-1 + mu + x)*y)/
       Power(Power(-1 + mu + x,2) + Power(y,2),2.5) + 
      (3*(1 - mu)*(mu + x)*y)/Power(Power(mu + x,2) + Power(y,2),2.5))/
    Sqrt(mu/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) + 
      (1 - mu)/Power(Power(mu + x,2) + Power(y,2),1.5)) - 
   (((-3*mu*y)/Power(Power(-1 + mu + x,2) + Power(y,2),2.5) - 
        (3*(1 - mu)*y)/Power(Power(mu + x,2) + Power(y,2),2.5))*
      (vy - (mu*(-1 + mu + x))/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) - 
        ((1 - mu)*(mu + x))/Power(Power(mu + x,2) + Power(y,2),1.5)))/
    (2.*Power(mu/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) + 
        (1 - mu)/Power(Power(mu + x,2) + Power(y,2),1.5),1.5));

res[2][2]=lambda;
res[2][3]=1/Sqrt(mu/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) + 
     (1 - mu)/Power(Power(mu + x,2) + Power(y,2),1.5));

// fode4

res[3][0]=-lambda - (y*((-3*mu*(-1 + mu + x))/
         Power(Power(-1 + mu + x,2) + Power(y,2),2.5) - 
        (3*(1 - mu)*(mu + x))/Power(Power(mu + x,2) + Power(y,2),2.5)))/
    Sqrt(mu/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) + 
      (1 - mu)/Power(Power(mu + x,2) + Power(y,2),1.5)) - 
   (((-3*mu*(-1 + mu + x))/Power(Power(-1 + mu + x,2) + Power(y,2),2.5) - 
        (3*(1 - mu)*(mu + x))/Power(Power(mu + x,2) + Power(y,2),2.5))*
      (-vx - y*(mu/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) + 
           (1 - mu)/Power(Power(mu + x,2) + Power(y,2),1.5))))/
    (2.*Power(mu/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) + 
        (1 - mu)/Power(Power(mu + x,2) + Power(y,2),1.5),1.5));
res[3][1]=(-(mu/Power(Power(-1 + mu + x,2) + Power(y,2),1.5)) - 
      (1 - mu)/Power(Power(mu + x,2) + Power(y,2),1.5) - 
      y*((-3*mu*y)/Power(Power(-1 + mu + x,2) + Power(y,2),2.5) - 
         (3*(1 - mu)*y)/Power(Power(mu + x,2) + Power(y,2),2.5)))/
    Sqrt(mu/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) + 
      (1 - mu)/Power(Power(mu + x,2) + Power(y,2),1.5)) - 
   (((-3*mu*y)/Power(Power(-1 + mu + x,2) + Power(y,2),2.5) - 
        (3*(1 - mu)*y)/Power(Power(mu + x,2) + Power(y,2),2.5))*
      (-vx - y*(mu/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) + 
           (1 - mu)/Power(Power(mu + x,2) + Power(y,2),1.5))))/
    (2.*Power(mu/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) + 
        (1 - mu)/Power(Power(mu + x,2) + Power(y,2),1.5),1.5));

res[3][2]=-(1/Sqrt(mu/Power(Power(-1 + mu + x,2) + Power(y,2),1.5) + 
       (1 - mu)/Power(Power(mu + x,2) + Power(y,2),1.5)));
res[3][3]=lambda;



return;
}



