#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/stat.h>
#include <homotopia.h>
//#include <mathlink.h>

#define HM_INT 1
#define HM_DOUBLE 2
#define HM_FLOAT 3

typedef struct ret_val_list
    {
    int ret_type; // type of value can be HM_INT, HM_DOUBLE, HM_FLOAT...
    int n;    // number of elements stored in the array pointed by dataptr
    void *dataptr; // pointer to array with "n" elements of type "ret_type"
    struct ret_val_list *next;
    } ret_val_list;

typedef struct
    {
    ret_val_list *first;
    int num;
    }ret_struct;

extern void getdim(int *nptr);
extern void getinitvals(int *dim,val_type *aldagaibalioak,int *pint, long pielem);

int stdlink = 0;
ret_struct retval;

int MLErrorMessage(int artificiallnk)
{
printf("alderantziz\n");
return(1);
}

void free_retval() { }

int MLPutFunction(int stdlink, char * cptr, int n) { return(1);}

int MLEndPacket(int stdlink) { return(1);}

int MLFlush(int stdlink) {return(1); }

void add_elem2list (ret_val_list * new)
{
int i;
ret_val_list **aux;

for (i = 0,aux = &(retval.first); *aux !=0; aux = &((*aux)->next),i++);
if (i != retval.num) printf("hostia, zerbait gaizki doa... itzulera egituran arazoak daude\n"); 
*aux = new;
new->next = 0;
retval.num ++;
}

MLPutReal64(int stdlink, double v)
{
int i;
ret_val_list *aux;
double *dptr;

aux = (ret_val_list *)malloc(sizeof(ret_val_list));
aux->ret_type = HM_DOUBLE;
aux->n = 1;
dptr =(double *)malloc(sizeof(double));
*dptr = v;
aux->dataptr = dptr;
add_elem2list (aux);
return(1);
}

int MLPutReal64List(int artificiallnk, double *vptr, int num)
{
int i;
ret_val_list *aux;
double *dptr;

aux = (ret_val_list *)malloc(sizeof(ret_val_list));
aux->ret_type = HM_DOUBLE;
aux->n = num;
dptr =(double *)malloc(num*sizeof(double));
for (i=0; i< num; i++) dptr[i] = vptr[i];
aux->dataptr = dptr;
add_elem2list (aux);
return(1);
}

#include <common-homotopia.c>

int main(int argc, char *argv[])
{
int n;
double t0;
double tf;
double *dptr,*dmik;
int *iptr,*imik;
long num;
val_type *vptr;
ret_val_list *aux;
int lmik;
char c;
int m;

retval.num = 0;
retval.first =0;

//t0 = 29.804721301317429;
t0 = 29.804721301268751;
//tf = t0-0.01;
tf=31;

if (argc ==3)
  {
  sscanf(argv[1],"%lf",&t0);
  sscanf(argv[2],"%lf",&tf);
  }

// mikel 07-02-2013
m=400;

dmik = (double *)malloc((2)*sizeof(val_type));
imik = (int *)malloc((2)*sizeof(val_type));
dmik[0]=0.01227747100000000; //mu
imik[0]=m; // m

vptr = (val_type *)malloc((8*m+9)*sizeof(val_type));

//getdim(&n);
getinitvals(&n,vptr,imik,1);



num = 0;
dptr =0;
iptr=0;
printf("Homotopia from %lf to %lf n = %d, first val = %lf last val = %lf\n",t0,tf,n,vptr[0],vptr[n-1]);


//mathHomotopia(vptr,(long)(n),t0,tf,dptr,num,iptr,num);
mathHomotopia(vptr,(long)(n),t0,tf,dmik,1,imik,1);



/* the solution is in the structure retval */
aux = retval.first;
printf("Homotopia ondoren, itzulitako elementu kopurua: %d\n",retval.num);
dptr = (double *)aux->dataptr;  // pointer to V
printf("  lehenengo balioa =%lf last val = %lf\n",dptr[0],dptr[aux->n-1]);
aux = aux->next; // t0
dptr = (double *)aux->dataptr;  // pointer to t0
printf("  tartea = %lf-tik ",dptr[0]);
aux = aux->next; // t1
dptr = (double *)aux->dataptr;  // pointer to t1
printf(" %lf-ra \n",dptr[0]);
aux = aux->next; // ||f(x)||
dptr = (double *)aux->dataptr;  // pointer to ||f(x)||
printf("   ||f(x) = %lf\n",dptr[0]);
free_retval();

// mikel 07-02-2013
free(dmik);
free(imik);

return 0;
}


