#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>


val_type edofun(val_type tau,
                val_type pq[], 
                val_type mu, val_type TT, val_type res[])

{
val_type x,y,vx,vy,t;
val_type e;

x=pq[0];
y=pq[1];
vx=pq[2];
vy=pq[3];

res[0] = <* edofun[tau,{x, y, vx, vy, t}, {mu,TT}][[1]] *>;
res[1] = <* edofun[tau,{x, y, vx, vy, t}, {mu,TT}][[2]] *>;
res[2] = <* edofun[tau,{x, y, vx, vy, t}, {mu,TT}][[3]] *>;
res[3] = <* edofun[tau,{x, y, vx, vy, t}, {mu,TT}][[4]] *>;

return(e) ;
}


val_type Hfun(const val_type pq[], val_type mu)

{
val_type x,y,vx,vy;
val_type e;

x=pq[0];
y=pq[1];
vx=pq[2];
vy=pq[3];

e = <* Hfun[{x, y, vx, vy}, {mu}] *>;

return(e) ;
}



void Hfgrad(const val_type pq[4], val_type mu,val_type res[4])
{
val_type x,y,vx,vy;

x=pq[0];
y=pq[1];
vx=pq[2];
vy=pq[3];

res[0]= <* Hfdq[[1]] *>;

res[1]= <* Hfdq[[2]] *>;

res[2]= <* Hfdp[[1]] *>;

res[3]= <* Hfdp[[2]] *>;

return;

}

void fodegraf(const val_type pq[], val_type mu,val_type lambda,val_type res[4][4])
{
val_type x,y,vx,vy;

x=pq[0];
y=pq[1];
vx=pq[2];
vy=pq[3];


// fode1

res[0][0]=<* fode1dq[[1]] *>;
res[0][1]=<* fode1dq[[2]] *>;

res[0][2]=<* fode1dp[[1]] *>;
res[0][3]=<* fode1dp[[2]] *>;

// fode2

res[1][0]=<* fode2dq[[1]] *>;
res[1][1]=<* fode2dq[[2]] *>;

res[1][2]=<* fode2dp[[1]] *>;
res[1][3]=<* fode2dp[[2]] *>;

// fode3

res[2][0]=<* fode3dq[[1]] *>;
res[2][1]=<* fode3dq[[2]] *>;

res[2][2]=<* fode3dp[[1]] *>;
res[2][3]=<* fode3dp[[2]] *>;

// fode4

res[3][0]=<* fode4dq[[1]] *>;
res[3][1]=<* fode4dq[[2]] *>;

res[3][2]=<* fode4dp[[1]] *>;
res[3][3]=<* fode4dp[[2]] *>;



return;
}



